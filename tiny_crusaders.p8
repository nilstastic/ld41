pico-8 cartridge // http://www.pico-8.com
version 16
__lua__

color = {
    dirt=4,
    bg=1,
    grass=3
}

-- *** dirty rect ***
blocks = {}
bufferaddr = 0x4300
function storeblock(bx,by,bw,bh)
	local bufferoffset = 0
	if (#blocks > 0) then
		bufferoffset = blocks[#blocks].bufferoffset + blocks[#blocks].length
	end
	
	for y=0,bh do
		local length = bw/2
		memcpy(bufferaddr+bufferoffset+y*length,0x6000+(by+y)*64+bx/2, length)
	end
	
	local block = {x=bx,y=by,w=bw,h=bh, bufferoffset=bufferoffset, length = bh*bw/2}
	add(blocks,block)
	return #blocks
end

function restoreblock(id)
	local block = blocks[id]
	for y=0,block.h-1 do
		local length = block.w/2
		memcpy(0x6000+(block.y+y)*64+block.x/2,bufferaddr+block.bufferoffset+y*length,length)
	end
end




--[[
88b           d88         db         88888888ba   
888b         d888        d88b        88      "8b  
88`8b       d8'88       d8'`8b       88      ,8p  
88 `8b     d8' 88      d8'  `8b      88aaaaaa8p'  
88  `8b   d8'  88     d8yaaaay8b     88""""""'    
88   `8b d8'   88    d8""""""""8b    88           
88    `888'    88   d8'        `8b   88           
88     `8'     88  d8'          `8b  88           
--]]


                  

function generate_map(size)
    local map = {}
    for y=0,size do
        map[y] = {} 
        for x=0,size do
            if (y < size-1 and y > 3) map[y][x] = flr(rnd()*2)
        end    
    end
    return map
end

function iterate_map(map)
    for y=0,#map do
        for x=0,#map[y] do
            local adjacent = count_adjacent(x,y,map)
            if adjacent > 3 then
                map[y][x] = 1
            elseif(8-adjacent > 3) then
                map[y][x] = 0
            end
        end    
    end
    return map
end

function count_adjacent(x,y,map)
    local count = 0
    for yy=y-1,y+1 do
        for xx=x-1,x+1 do
            --if yy > 0 and xx > 0 and xx < #map[0] and yy < #map and map[yy][xx] > 0 then
            if map[yy] != nil and map[yy][xx] != nil and map[yy][xx] > 0 then
                count+=1
            end
        end
    end

    return count-1
end

function draw_map(map)
    local blocksize = 128/#map

    for y=0,#map do
        for x=0,#map[y] do
            
            if (map[y][x] >= 1) then
                draw_block(x,y,blocksize, color.dirt)
            else
                --draw_block(x,y,blocksize, color.bg)
            end
        end    
    end
end

function draw_block(x,y,size,color)
    pset(x*size,y*size,color)
    pset(x*size+1,y*size,color)
    pset(x*size+1,y*size+1,color)
    pset(x*size,y*size+1,color)
end

function smooth_map()
    for y=0,127 do
        for x=0,127 do
            local adjacent = count_adjacent_pixels(x,y,color.bg)
            if adjacent == 4 then
                pset(x,y,color.dirt)
            end
        end    
    end
end

function count_adjacent_pixels(x,y,color)
    local count = 0
    for yy=y-1,y+1 do
        for xx=x-1,x+1 do
            --if yy < 0 or xx < 0 or xx > 128 or yy > 128 or pget(xx,yy) != color then
            if yy > 0 and xx > 0 and xx < 128 and yy < 128 and pget(xx,yy) != color then
                count+=1
            end
        end
    end

    return count-1
end

function make_map(iterations)
    local map = generate_map(64)
    
    for i=0,iterations do
        map = iterate_map(map)
    end

    cls(color.bg)
    draw_map(map)
    
    smooth_map()
    addgrass()
end

--[[
                                                                        
  ,ad8888ba,   88888888ba          db          ad88888ba    ad88888ba   
 d8"'    `"8b  88      "8b        d88b        d8"     "8b  d8"     "8b  
d8'            88      ,8p       d8'`8b       y8,          y8,          
88             88aaaaaa8p'      d8'  `8b      `y8aaaaa,    `y8aaaaa,    
88      88888  88""""88'       d8yaaaay8b       `"""""8b,    `"""""8b,  
y8,        88  88    `8b      d8""""""""8b            `8b          `8b  
 y8a.    .a88  88     `8b    d8'        `8b   y8a     a8p  y8a     a8p  
  `"y88888p"   88      `8b  d8'          `8b   "y88888p"    "y88888p"   
                                                                        
                                                                        
--]]

function addgrass()
    for y=0,127 do
        for x=0,127 do
            local adj = count_adjacent_pixels(x,y,color.dirt)
            if pget(x,y) == color.dirt and rnd() > 0.94 then
                pset(x,y,2)
            end
            -- if  pget(x,y) == color.bg and pget(x,y+1) == color.dirt then
            --     pset(x,y,color.grass)
            -- end

            if  pget(x,y) == color.bg and pget(x,y+1) == color.dirt then
                pset(x,y,color.grass)
                line(x,y+1,x,flr(y+rnd()*4)+1,2)
            end
        end    
    end
end

--[[
                                                                                   
 ad88888ba   88888888ba          db         i8,        8        ,8i  888b      88  
d8"     "8b  88      "8b        d88b        `8b       d8b       d8'  8888b     88  
y8,          88      ,8p       d8'`8b        "8,     ,8"8,     ,8"   88 `8b    88  
`y8aaaaa,    88aaaaaa8p'      d8'  `8b        y8     8p y8     8p    88  `8b   88  
  `"""""8b,  88""""""'       d8yaaaay8b       `8b   d8' `8b   d8'    88   `8b  88  
        `8b  88             d8""""""""8b       `8a a8'   `8a a8'     88    `8b 88  
y8a     a8p  88            d8'        `8b       `8a8'     `8a8'      88     `8888  
 "y88888p"   88           d8'          `8b       `8'       `8'       88      `888  
                                                                                   
                                                                                   
--]]

crusaders = {}
crusaders_waiting = {}

function get_spawn_position()
    local point = {x=127,y=127}
    for x=16, 127-16 do
        for y=8, 127 do
            if pget(x,y) != color.bg and y < point.y then
                point.y = y
                point.x = x
            end
        end
    end

    return point
end

crusader_spawn_count = 0
function crusader_spawn(x,y,lx,count,interval)
    crusader_spawn_count += count
    local c = cocreate(function()
        for c=1,count do
            add(crusaders,{x=x,y=y,dx=0,dy=0,lx=lx,action=action_walk,fall=0,animationspeed=0.5,animationframe=0,animationoffset=0})
            crusader_spawn_count -=1
            for i=0,interval do
                yield()
            end
        end
    end)
    add(actions,c)
end

function water_spawn(x,y,lx,count,interval)

    local c = cocreate(function()
        for c=1,count do
            add(monsters,{x=rnd()*128,y=y,dx=0,dy=0,lx=lx,action=monster_walk,fall=0,animationspeed=0.5,animationframe=0,animationoffset=12})
            for i=0,interval do
                yield()
            end
        end
    end)
    add(actions,c) 
end

function generic_spawn(x,y,lx,count,interval)

    local c = cocreate(function()
        for c=1,count do
            add(monsters,{x=x+flr(rnd()*4-4),y=y,dx=0,dy=0,lx=lx,action=monster_walk,fall=0,animationspeed=0.5,animationframe=0,animationoffset=13})
            for i=0,interval do
                yield()
            end
        end
    end)
    add(actions,c) 
end


function spawn_dirt(x,y,lx)
    add(monsters,{x=x,y=y,dx=0,dy=0,lx=lx,action=monster_walk,fall=0,animationspeed=0.5,animationframe=0,animationoffset=13})
end

function spawn_monster(x,y,lx)
    add(monsters,{x=x,y=y,dx=0,dy=0,lx=lx,action=monster_walk,fall=0,animationspeed=0.5,animationframe=0,animationoffset=12})
end

function get_spawn_friend()
    for i=0, 100 do
        local x = flr(rnd()*127)
        local y = flr(rnd()*96+32) - 32

        if pget(x,y) != color.bg then
            for dy=y,y-20,-1 do
                --pset(x,dy,9)
                if pget(x,dy) == color.bg then
                    return {x=x-4,y=dy}
                end
                            
            end
        end
    end

end

function add_animation(x,y,loop)
--add(crusaders,{x=x,y=y,dx=0,dy=0,lx=lx,action=action_walk,fall=0,animationspeed=0.5,animationframe=0,animationoffset=0})
           
    --add(envanimations, {x=x,y=y,animationoffset=16, animationframes=6, animationspeed=0.25, animationframe=0,loop=loop})
end

--[[
       db           ,ad8888ba,   888888888888  88    ,ad8888ba,    888b      88  
      d88b         d8"'    `"8b       88       88   d8"'    `"8b   8888b     88  
     d8'`8b       d8'                 88       88  d8'        `8b  88 `8b    88  
    d8'  `8b      88                  88       88  88          88  88  `8b   88  
   d8yaaaay8b     88                  88       88  88          88  88   `8b  88  
  d8""""""""8b    y8,                 88       88  y8,        ,8p  88    `8b 88  
 d8'        `8b    y8a.    .a8p       88       88   y8a.    .a8p   88     `8888  
d8'          `8b    `"y8888y"'        88       88    `"y8888y"'    88      `888  
--]]
actions = {}
function action_walk(crusader)

	if pget(crusader.x,crusader.y+2) == color.bg or crusader.y > 120 then
		-- fall
		crusader.dy = 2
        crusader.fall += 1
        if(abs(crusader.dx > 0.1)) crusader.dx -= crusader.dx / 10
	elseif pget(crusader.x+crusader.lx,crusader.y+0) != color.bg then
		-- wall
		crusader.lx *=-1
		crusader.dy = 0 
		crusader.dx = 0
	elseif pget(crusader.x+crusader.lx,crusader.y+2) == color.bg then
		-- downhill
		crusader.dy = 1
		crusader.dx = 1
        if not crusader_dead_fall(crusader) then
            if(crusader.fall > 3) sfx(5)
            crusader.fall = 0
        end
	elseif pget(crusader.x+crusader.lx,crusader.y+1) != color.bg then
		-- uphill
		crusader.dy = -1
		crusader.dx = 1
        if not crusader_dead_fall(crusader) then
            if(crusader.fall > 3) sfx(5)
            crusader.fall = 0
        end
	elseif pget(crusader.x,crusader.y+2) != color.bg then
		-- flat 
		crusader.dy = 0
		crusader.dx = 1

        if not crusader_dead_fall(crusader) then
            if(crusader.fall > 3) sfx(5)
            crusader.fall = 0
            
        end
	end

	crusader.x += crusader.dx * crusader.lx 
	crusader.y += crusader.dy

end


function monster_walk(monster)
  
	if pget(monster.x,monster.y+2) == color.bg or pget(monster.x,monster.y+2) == 12 or monster.y > 120 then
		-- fall
		monster.dy = 2
        monster.fall += 1
        if(abs(monster.dx > 0.1)) monster.dx -= monster.dx / 10
	elseif pget(monster.x+monster.lx,monster.y+0) != color.bg then
		-- wall
		monster.lx *=-1
		monster.dy = 0 
		monster.dx = 0
	elseif pget(monster.x+monster.lx,monster.y+2) == color.bg then
		-- -- downhill
		monster.dy = 1
		monster.dx = 1

	-- elseif pget(monster.x+monster.lx,monster.y+1) != color.bg then
	-- 	-- uphill
	-- 	monster.dy = -1
	-- 	monster.dx = 1
	elseif pget(monster.x,monster.y+2) != color.bg then
		-- flat 
		monster.dy = 0
		--monster.dx = 0
		monster.dx = 1
	end

	monster.x += monster.dx * monster.lx 
	monster.y += monster.dy

end


function crusader_dead_fall(crusader)
    if(crusader.fall > 15) then
        for x=crusader.x-2, crusader.x+2 do
            for y=crusader.y-4, crusader.y+4 do
                if(pget(x,y) != color.bg) then
                    sfx_blood(x,y,20)
                    break
                end
            end 
        end
        
        --sfx(0)
        sfx(6)
        crusader_kill(crusader)
        
        return true

        --sfx(6)
    end

    

    return false
end

function crusader_kill(crusader)
    del(crusaders,crusader)
    state.crusaders_alive -=1

    add_animation(crusader.x,crusader.y)
end

function sfx_blood(x,y,length)
    local c = cocreate(function()
        local s = rnd()
        for i=1,length do
            line(x,y,x,y + i*s,8)
            s *= 0.99
            
            yield()
        end



        end)
    add(actions,c)
end

--[[
88        88  88  
88        88  88  
88        88  88  
88        88  88  
88        88  88  
88        88  88  
y8a.    .a8p  88  
 `"y8888y"'   88  
--]]

uiactions = {}
function show_title()
    cls()
   
    -- spr(7,44,10,5,2) 
    -- spr(32,18,24,11,2)

    local c = cocreate(function()
        local s = rnd()
        local steps = 15
        for i=1,steps do
            
            
            spr(7,44,10,5,2/steps*i) 
            spr(32,18,24,12/steps*i,2)
            
            --yield() 
            yield()
        end
        end)
    add(uiactions,c)

    local infopos = {x=20,y=50}
    print_shadow("+ click to start",infopos.x,infopos.y,7)
    print_shadow("+ click to dig",infopos.x,infopos.y+10,7)
    print_shadow("+ click to be a hero!",infopos.x,infopos.y+20,7)

    

end

function show_gameover(y)
    cls()
    gameover_visible = true
    print_shadow("the crusade has failed", 20,65+y,8,1)
    print_shadow("you made it to cave "..state.level_count, 20,35+y,7)
    print_shadow(state.friend_saved.." crusaders were saved", 20,45+y,7)
    --print_shadow("all "..(state.friend_saved+crusader_startcount).." were lost", 0,65+y,7)
    print_shadow("(click to crusade some more)", 7,110+y,12,0)
    
end

function print_shadow(text,x,y,c,s)
    if (s == nil) s = 5
    print(text,x,y+1,s)
    print(text,x,y,c)
end

function draw_ui(text,x,y)
    print(text,x+1,y,5)
    print(text,x,y,7)
end

function reset_game()
    gameover = true
    
    -- remove all pending actions
    for c in all(actions) do
        del(actions,c) 
    end

    crusaders_waiting = {}
    blocks = {}
end

function restart_game()
    gameover_visible = false
    state.friend_saved = 0
    state.level_count = 1
    game_started = true
 
    for c in all(actions) do
        del(actions,c) 
    end
    blocks = {}
    gameover = false
    state.level_seed = 1
    
 

    envanimations = {}


    next_level(crusader_startcount, seeds[state.level_count])
end

function next_level(count,seed)
    
    
    
    state.crusaders_alive = count
    state.crusader_saved = 0
    crusaders = {}

    monsters = {}


    srand(seed)
    --srand(levels[level])
    
    --if(state.level_count==1) music(0)
    make_map(3)

    if(state.level_count==1) sfx(3)
    local spawn_position = get_spawn_position()
    local lx = 1
    if (spawn_position.x > 64) lx = -1
    crusader_spawn(spawn_position.x,spawn_position.y-8,lx,count,crusader_interval)

    -- WATER 
    water_spawn(r,0,1,60,10)

  

    if(state.level_count<2) return


    local friend = get_spawn_friend()
    for crusader in all(crusaders_waiting) do 
        del(crusader)
    end
    crusaders_waiting = {}
    
     for anim in all(envanimations) do 
        del(anim)
    end

    envanimations = {}
    
    add(crusaders_waiting,{x=friend.x,y=friend.y-3,dx=0,dy=0,lx=0,action=action_walk,fall=0,animationspeed=0.5,animationframe=0})

    
    --spr(1,spawnfriend.x, spawnfriend.y-8)
end

--[[
88  888b      88  888888888888  88888888888  88888888ba          db           ,ad8888ba,   888888888888  
88  8888b     88       88       88           88      "8b        d88b         d8"'    `"8b       88       
88  88 `8b    88       88       88           88      ,8p       d8'`8b       d8'                 88       
88  88  `8b   88       88       88aaaaa      88aaaaaa8p'      d8'  `8b      88                  88       
88  88   `8b  88       88       88"""""      88""""88'       d8yaaaay8b     88                  88       
88  88    `8b 88       88       88           88    `8b      d8""""""""8b    y8,                 88       
88  88     `8888       88       88           88     `8b    d8'        `8b    y8a.    .a8p       88       
88  88      `888       88       88888888888  88      `8b  d8'          `8b    `"y8888y"'        88       
--]]
function friend_close(friend)
    for crusader in all(crusaders) do
        local dx,dy = crusader.x-friend.x, crusader.y-friend.y
        if(abs(dx) < 10 and abs(dy)<10) then
            add(crusaders, friend)
            state.crusaders_alive += 1
            friend.lx = crusader.lx
            --del(crusaders_waiting, friend)
            crusaders_waiting = {}
            state.friend_saved +=1
             sfx(7)
            -- = {}
            break;
        end
    end
end

--[[
88  888b      88  88  888888888888  
88  8888b     88  88       88       
88  88 `8b    88  88       88       
88  88  `8b   88  88       88       
88  88   `8b  88  88       88       
88  88    `8b 88  88       88       
88  88     `8888  88       88       
88  88      `888  88       88       
--]]
state = {
    crusaders_alive = 0,
    --level_seed = 1,
    level_count = 0,
    crusader_saved = 0, 
    friend_saved = 0
}

gameover_visible = false

envanimations = {}

-- mouse
mx = 0
my = 0

-- current frame
f = 0

-- a few easy level seeds
seeds = {
2,6,19,23,24,25,26,53,57,58,59,63,64,69,70,71,72,73,78,81,85,92,99
    }

crusader_startcount = 8
crusader_interval = 30
game_started = false
gameover = true 

goodlevels = {}

function _init()
printh("nit")
    -- enable mouse
	poke(0x5f2d, 1)
    cls(color.bg)

    -- add a few random levels?
    for i=100,200 do
        --add(levels,flr(rnd()*1000)+50)
        add(seeds,i)
    end

    --next_level(crusader_startcount, levels[state.level_count])
end


--[[
88        88  88888888ba   88888888ba,           db         888888888888  88888888888  
88        88  88      "8b  88      `"8b         d88b             88       88           
88        88  88      ,8p  88        `8b       d8'`8b            88       88           
88        88  88aaaaaa8p'  88         88      d8'  `8b           88       88aaaaa      
88        88  88""""""'    88         88     d8yaaaay8b          88       88"""""      
88        88  88           88         8p    d8""""""""8b         88       88           
y8a.    .a8p  88           88      .a8p    d8'        `8b        88       88           
 `"y8888y"'   88           88888888y"'    d8'          `8b       88       88888888888  
--]]
gameoverpos = 90
current_gameoverpos = 90
function _update()

        mx = stat(32) 
        my = stat(33)

    if (game_started==false) then
        show_title()
        
        -- run all coroutines
        for c in all(uiactions) do
            if (not coresume(c) or gameover) del(actions,c) 
        end

        -- click to start the game
        if (stat(34) == 1) then
            restart_game()
        end

        return
        elseif(gameover) then
            if(gameover_visible==false) then
                sfx(4)
                current_gameoverpos = gameoverpos
            end

        if (current_gameoverpos>0) current_gameoverpos-=2
        show_gameover(current_gameoverpos)
        for anim in all(monsters) do 
            del(anim)
        end
        if (stat(34) == 1) then
            restart_game()
        end
        return
    else
    
        -- check game over
        if #crusaders < 1 and crusader_spawn_count < 1 and state.crusader_saved < 1 and game_started then
            gameover = true
            --show_gameover()
        elseif #crusaders < 1 and crusader_spawn_count < 1 and state.crusader_saved > 0 then
            for c in all(actions) do
                del(actions,c) 
            end
            blocks = {}
            monsters = {}
            gameover = false
            state.level_count += 1
            next_level(state.crusader_saved, seeds[state.level_count])
        end

        -- restart the game
        if btn(4) then 
            restart_game()
        end

         if btn(5) then 
            --printh(state.level_count)
            printh(#monsters)
            gameover = true
        end

        -- restore the background
        for i=1,#blocks do
            restoreblock(i)
        end
        blocks = {}

        if (gameover) return

        -- run all coroutines
        for c in all(actions) do
            if (not coresume(c) or gameover) del(actions,c) 
        end

        -- draw tunnel
        if stat(34) == 1 and my < 125 then
            if(pget(mx-4,my) != color.bg or pget(mx+4,my) != color.bg) then 
                spawn_dirt(mx,my-3,1)
            end
            circfill(mx,my,2,color.bg)

            
        end
--cls()
   


        for monster in all(monsters) do
            if(f%1==0) monster.action(monster)
            storeblock(monster.x, monster.y, 2, 2)

            if (monster.y > 122) then
                del(monsters,monster)
            end
        end




        -- update waiting crusaders 
        for crusader in all(crusaders_waiting) do
            friend_close(crusader)
            storeblock(crusader.x-3, crusader.y-5, 8, 8)
        end
        
        -- update crusaders
        for crusader in all(crusaders) do
            if(f%1==0) crusader.action(crusader)
            crusader.animationframe+=crusader.animationspeed
            
            if(crusader.y < 124) then
                storeblock(crusader.x-3, crusader.y-5, 8, 8)
            else
                storeblock(0, 118, 128, 10)
            end
            
            if (crusader.y > 130) then
                del(crusaders,crusader)
                state.crusader_saved +=1
            end

        end

        --  for anim in all(envanimations) do
        --     storeblock(anim.x, anim.y, 8, 8)
        -- end
    end

        --  for anim in all(envanimations) do
        --     storeblock(anim.x, anim.y, 8, 8)
        -- end

    -- update mouse
    if mx > 1 and mx < 127 and my > 1 and my < 122 then
        storeblock(stat(32)-1,stat(33)-1, 4, 4)
    end

    -- ui position
    storeblock(4,4,48,8)
    storeblock(4,96,48,8)

    f+=1 
end


function _draw()

    --spr(5,88,24)


    if game_started and f > 4 then
        for crusader in all(crusaders_waiting) do
            spr(2+flr(crusader.animationframe)%2,crusader.x-3, crusader.y-5,1,1,crusader.lx>0,false)
        end

        for crusader in all(crusaders) do
            spr(flr(crusader.animationframe)%3,crusader.x-3, crusader.y-5,1,1,crusader.lx>0,false)
        end
        
        --if f > 4 then
            for monster in all(monsters) do
                spr(monster.animationoffset,monster.x, monster.y)
            end

             draw_ui("level:"..state.level_count,94,4)
            draw_ui("heroes:"..state.crusaders_alive,4,4)
        --end
       
        --draw_ui("seed:"..seeds[state.level_count],94,4)
    
        -- for anim in all(envanimations) do
        --     spr(anim.animationoffset + (f*0.15%7),anim.x-3, anim.y-5)
        --     anim.animationframe+=anim.animationspeed

        --     if (f*0.15%6)>anim.animationframes then
        --         del(envanimations,anim)
        --     end
        -- end

    end
    
    if mx > 1 and mx < 127 and my > 1 and my < 122 then
        pset(mx+1,my,6)
        pset(mx-1,my,6)
        pset(mx,my+1,6)
        pset(mx,my-1,6)
    end


    --draw_ui("heroes:"..#crusaders,4,4)
    --draw_ui("saved:"..saved,4,10)
end

__gfx__
000fff00000fff00000fff00060444000604440000000000000000000000000000000000000000000000000000000000c0000000400000000000000000000000
060fff00060fff00060fff00060f4f00060f4f000006650000000000000000000000000000000000000000000000000000000000000000000000000000000000
060f6660060f6660060f6660060ff660060ff6600066665000000000000777777770777700777700777007777077770000000000000000000000000000000000
0607686006076860060768600f7776600f777660006dd650000000000007dddddd707dd7007ddd70dd7007dd707dd70000000000000000000000000000000000
0f7788800f7788800f778880000776600007766000666650000000000007cccccc707cc7007cccc7cc7007cc707cc70000000000000000000000000000000000
000768600007686000076860000ccc00000ccc000066665000000000000777cc77707cc7007ccccccc7007cc777cc70000000000000000000000000000000000
0006060000006600000006000000c00000000c000066665000000000000557cc75507cc7007ccccccc7007ccdddcc70000000000000000000000000000000000
00000000000000000000000000000000000000003333333300000000000007cc70007cc7007cc7cccc7005777c77750000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000007cc70007cc7007cc57ccc7000557c75500000000000000000000000000000000000
000fff00000fff00000fff00000fff00000fff000000000000000000000007777000777700777057777000007770000000000000000000000000000000000000
060fff00060fff00060fff00860fff00088888800000000000000000000005555000555500555005555000005550000000000000000000000000000000000000
060f6660068f6668080f666888888880000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
06076860060768600608886000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0f7788880f8788800f88888800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00076860008888880000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00888888000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
77777777700777777777700777707777007777777777007777777770077777777000777777770077777777770000000000000000000000000000000000000000
72222222700722222222700722707227007222222227007222222270072222222700722222270072222222270000000000000000000000000000000000000000
78888888700788888888700788707887007888888887007888888870078888888700788888870078888888870000000000000000000000000000000000000000
78888888700788777788700788707887007887777887007887778870078888888700788777770078877778870000000000000000000000000000000000000000
78877777700788755788700788707887007887557887007887578870078877888700788755550078875578870000000000000000000000000000000000000000
78875555500788777788700788707887007887007777007887078870078875788700788777000078877778870000000000000000000000000000000000000000
78870000000788222288700788707887007887005555007887778870078870788700788227000078822228870000000000000000000000000000000000000000
78870000000788888888700788707887007777777777007882228870078870788700788887000078888888870000000000000000000000000000000000000000
78870000000788888777500788707887005555557227007888888870078877288700788777000078888877750000000000000000000000000000000000000000
78877777700788788275000788777887007777007887007888888870078822888700788755000078878827500000000000000000000000000000000000000000
78822222700788778827000788888887007227777887007887778870078888888700788777770078877882700000000000000000000000000000000000000000
78888888700788757882700788888887007882222887007887578870078888888700788222270078875788270000000000000000000000000000000000000000
78888888700788705788700788888887007888888887007887078870078888888700788888870078870578870000000000000000000000000000000000000000
77777777700777700777700777777777007777777777007777077770077777777500777777770077770077770000000000000000000000000000000000000000
55555555500555500555500555555555005555555555005555055550055555555000555555550055550055550000000000000000000000000000000000000000
__sfx__
000100001062002620026200262003620046201262013620176201b62022620236202462024620246202562025620256200762007620076200762007620066200562005620056200562004620046200362004620
0002000033050300502c0502905026050240502205021050200501f0501e0501d0501c0501b0501b0501a050180501605015050110500f0500c0500a050080500405001050010000100001000010000200000000
00100000000000000000000000000000000000000001e030100501105012050130501305013050130501305013050130501205012050110501300034000150001600000000000000000000000000000000000000
00100000000000c3300e330000000f3501235015350163501835019350193501a35019350173501635015350133501c3502135000000000002a35029350000000000000000000000000000000000000000000000
001000002e0502a050220501c0501a0501605013050100500e0500b05007050050500305001030010200101001000010000200002000020000200002000020000200002000020000200002000020000000000000
000100000e0200e0200d0200b03008030020300403004020040200302003010020100101001010070100701008010080100800008000070000600005000020000200002000020000200002000020000200002000
000100003d0503805034050300502b050250501e05018050110500a050030500105012050120501305012050120500f0500d0500b050070500405003050020500205002050020500205002050020500205002050
0003000000000000000000000000000000000000000000000000000000000000f050150501f050230502405028050280503d0503b050320502b05032050000000000000000300502c05000000000000000000000
__music__
00 05010444

