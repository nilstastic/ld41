pico-8 cartridge // http://www.pico-8.com
version 16
__lua__

color = {
    dirt=4,
    bg=1
}
-- *** MAP ***
function generate_map(size)
    local map = {}
    for y=0,size do
        map[y] = {} 
        for x=0,size do 
            map[y][x] = flr(rnd()*2)
            --map[y][x] = flr(rnd()*1.6 + (0.4/height)*y)
        end    
    end
    return map
end

function iterate_map(map)
    for y=0,#map do
        for x=0,#map[y] do
            local adjacent = count_adjacent(x,y,map)
            if adjacent > 3 then
                map[y][x] = 1
            elseif(8-adjacent > 3) then
                map[y][x] = 0
            end
        end    
    end
    return map
end

function count_adjacent(x,y,map)
    local count = 0
    for yy=y-1,y+1 do
        for xx=x-1,x+1 do
            --if yy > 0 and xx > 0 and xx < #map[0] and yy < #map and map[yy][xx] > 0 then
            if map[yy] != nil and map[yy][xx] != nil and map[yy][xx] > 0 then
                count+=1
            end
        end
    end

    return count-1
end

function count_adjacent2(x,y,color,map)
    local count = 0
    for yy=y-1,y+1 do
        for xx=x-1,x+1 do
            if map[yy] != nil and map[yy][xx] != nil and map[yy][xx] == color then
                count+=1
            end
        end
    end

    return count-1
end

function expand_map(map, blocksize)
    local expanded_map = {}
    for y=0, 128 do
        expanded_map[y] = {}
        for x=0, 128 do
            mx = flr(x / 2)
            my = flr(y / 2)
            --print()
            expanded_map[y][x] = map[my][mx]
        end
    end

    return expanded_map
end

function draw_map(map)
    local blocksize = 128/#map

    for y=0,#map do
        for x=0,#map[y] do
            
            if (map[y][x] >= 1) then
                draw_block(x,y,blocksize, color.dirt)
            else
                --draw_block(x,y,blocksize, color.bg)
            end
        end    
    end
end

function draw_map2(map)
    for y=0,#map do
        for x=0,#map[y] do
            if (map[y][x] >= 1) then
                draw_block(x,y,1, color.dirt)
            end
        end    
    end
end


function draw_block(x,y,size,color)
    pset(x*size,y*size,color)
    pset(x*size+1,y*size,color)
    pset(x*size+1,y*size+1,color)
    pset(x*size,y*size+1,color)
end

function smooth_map2(map)
    for y=0,127 do
        for x=0,127 do
            local adjacent = count_adjacent2(x,y,color.bg,map)
            if adjacent == 4 then
                map[y][x] = color.dirt
                --pset(x,y,color.dirt)
            end
        end    
    end
    return map
end


function smooth_map()
    for y=0,127 do
        for x=0,127 do
            local adjacent = count_adjacent_pixels(x,y,color.bg)
            if adjacent == 4 then
                pset(x,y,color.dirt)
            end
        end    
    end
end

function count_adjacent_pixels(x,y,color)
    local count = 0
    for yy=y-1,y+1 do
        for xx=x-1,x+1 do
            --if yy < 0 or xx < 0 or xx > 128 or yy > 128 or pget(xx,yy) != color then
            if yy > 0 and xx > 0 and xx < 128 and yy < 128 and pget(xx,yy) != color then
                count+=1
            end
        end
    end

    return count-1
end
-- create and draw a map
function make_map(iterations)
    local map = generate_map(64)
    
    for i=0,iterations do
        map = iterate_map(map)
    end

    --print("done")
    cls(color.bg)

    draw_map(map)
    smooth_map()


    -- map = expand_map(map)
    -- map = smooth_map2(map)
    -- draw_map2(map)
    
end

function _init()
    --srand(1)
    cls(color.bg)
    make_map(3)
    -- draw_map(map)
    --print(stat(95))
end

f = 0
function _update()
    --if (f%30==0) make_map(3)
    f+=1 
end
